//import thư viện mongoose
const mongoose = require("mongoose");
//import thư viện schema class
const Schema = mongoose.Schema;

//tạo class voucher history schema
const voucherHistorySchema = new Schema ({
    username: {
        type: String, 
        ref: "User", 
        required: true
    },
	voucher: {
        type: String,
        ref: "Voucher",
        required: true
    },
    createdAt: {
        type: Date, 
        default: Date.now()
    },
    updatedAt: {
        type: Date, 
        default: Date.now()
    }
});
module.exports = mongoose.model("voucherhistory", voucherHistorySchema);