//khai báo thư viện express Js
const express = require("express");
//khai báo router chạy
const router = express.Router();
const diceController = require("../controllers/diceController");

router.post("/devcamp-lucky-dice/dice",diceController.diceHandler);

module.exports = router;